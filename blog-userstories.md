# Flask. Sample User Stories


## 	Blogging Engine with Authentication in Flask


### Personas

1. Blogger: The person who writes blog posts
2. Reader: The person who reads blog posts


### User Stories
* As a reader *ISBAT see all blogs.
* As a reader *ISBAT register and register myself as a blogger. (Sign Up)
* As a reader *ISBAT login and become blogger. (Authentication)
* As a blogger *ISBAT create a blog post.
* As a blogger *ISBAT delete my own blog post.
* As a blogger *ISBAT update my blog post.
* As a reader *ISBAT see an error page if I enter wrong URL. (Error Handling)

#### Bonus:
* As a reader *ISBAT search any text. (Full text Search)
* As a reader *ISBAT export blogpost as text file. (Use Celery for background jobs)

\* ISBAT = I should be able to.